var gulp = require('gulp');
var connect = require('gulp-connect');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var watch = require('gulp-watch');
var imagemin = require('gulp-imagemin');
var clean = require('gulp-clean');
var minifyCSS = require('gulp-minify-css');
var notify = require('gulp-notify');
var pug = require('gulp-pug2');
var sourcemaps = require('gulp-sourcemaps');


/**
 * Specify config for tasks
 */
var config = {

	  // SRC
    pug: 'src/*.pug',
    scss: 'src/scss/**/*.scss',
    images: 'src/images/**/*',
    fonts: 'src/fonts/*',
    css:[
        'src/css/**/*.css',
        'node_modules/owl.carousel/dist/assets/owl.carousel.min.css',
    ],
    js:
    [
        'node_modules/jquery/dist/jquery.js',
        'src/js/jquery.easing.1.3.js',
        'src/js/jquery.visible.min.js',
        'src/js/jquery.backgroundparallax.min.js',
        'node_modules/owl.carousel/dist/owl.carousel.min.js',
        'src/js/main.js',
        'src/js/svgcheckbox.js',
    ],

    // DIST
    dist: 'dist',
    distJs: 'dist/js',
    distCss: 'dist/css',
    distImages: 'dist/images',
    distFonts: 'dist/fonts',

    //ROOT
    rootFolder: './',
};

/**
 * Clear the destination folder
 */
gulp.task('clean', function () {
    gulp.src('dist/**/*.*', { read: false })
        .pipe(clean({ force: true }));
});

/**
 *  Copy all application files except *.sass .js and .html into the `dist` folder
 */
gulp.task('copy', function () {

    //copy fonts
    gulp.src(config.fonts)
        .pipe( gulp.dest(config.distFonts)),

    //copy and minify images
    gulp.src(config.images)
        .pipe(imagemin())
        .pipe(gulp.dest(config.distImages));
});

/**
 *  Copy all application files except *.sass .js and .html into the `dist` folder
 */
gulp.task('views', function() {
    return gulp.src(config.pug)
        .pipe(pug())
        .pipe(gulp.dest(config.dist))
        .pipe(notify('Views done!'));
});

/**
 * build js files into one bundle
 */
gulp.task('scripts', function() {
  return gulp.src(config.js)
    .pipe(sourcemaps.init())
    .pipe(concat('bundle.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(config.distJs))
    .pipe(notify('scripts done!'));
});

/**
 * compile sass into css and build into one bundle 
 */
gulp.task('scss', function () {
  return gulp.src(config.scss)
    // .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
        browsers: ['last 15 versions'],
        cascade: true
    }))
    .pipe(concat('scss.css'))
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest('src/css'));
});

/**
 * build js files into one bundle
 */
gulp.task('css', function() {
  return gulp.src(config.css)
    // .pipe(sourcemaps.init())
    .pipe(concat('bundle.min.css'))
    .pipe(minifyCSS())
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest(config.distCss))
    .pipe(connect.reload())
    .pipe(notify('Css Done!'));
});

 /**
 * watch task
 */
gulp.task('watch', function () {

  //index
  gulp.watch(config.pug, ['views']),

  //fonts
  gulp.watch(config.fonts, ['copy']),

  //images
  gulp.watch(config.images, ['copy']),

  //scss
  gulp.watch(config.scss, ['scss']),

  //css
  gulp.watch(config.css, ['css']),

  //js
  gulp.watch(config.js, ['scripts'])

});

/**
 * Start local server for development
 */
gulp.task('server', ['clean', 'views', 'copy', 'scripts', 'scss', 'css'], function () {

    /**
     * Listening port can be specified manually via command `PORT=7777 gulp`
     */
    var serverPort = process.env.PORT || 9000;

    connect.server({
        root: config.rootFolder,
        port: serverPort,
        livereload: true
    });

});

/**
 * Default gulp task
 */
gulp.task('default', [ 'server', 'watch']);

